import React,{useState,useEffect} from 'react'


import './formfields.css'
import Axios from "axios";

function Formfields() {

    const [layout,setLayoutState] = useState("");
    const [name,setName] = useState("");
    const [capacity,setCapacity]= useState("");
    const [chk,setChk] = useState("");
    const [img,setImg] = useState("");
    const [userlist,setUserList] = useState([""]);
    

    useEffect(() =>{
        Axios.get('http://localhost:8080/api/get').then((response) =>{
            setUserList(response.data);
        });
    },[]);





    const submittbl=() => {
        Axios.post('http://localhost:8080/api/insert',{layout:layout,name:name,capacity:capacity,chk:chk,img:img}).then(()=> {
            alert("successful insert");
        });



    };


    const deleteinfo =(name_del) =>{
        Axios.delete('http://localhost:8080/api/delete/${name_del}',{name_del:name_del});
    };


  


  return (
    <div className='container'>

      <form className='main-form'>

        <label id='title'>Create Table</label>

        <hr />
        
        <div className='inner-content'>
        <ul>
<label>Layout:</label> 
<select id='layout-select' onChange={(e) => {
    setLayoutState(e.target.value);
}}
>
<option value="">Select Layout</option>
<option value="linear">Linear Layout</option>
<option value="Relative">Relative Layout</option>
<option value="Constraint">Constraint Layout</option>
</select>
</ul>

<ul>
<label>Name:</label>
<input id='name' placeholder='Enter Name' onChange={(e) => {
    setName(e.target.value);
}}  
/>

</ul>
<ul>
<label>
Capacity:
</label>
<input  id='capacity' placeholder='Enter number of capacity' onChange={(e) =>{
    setCapacity(e.target.value);
}} />

</ul>

<ul>

<label>Status:</label>

<input id='chkbox' type="checkbox" checked="checked" onChange={(e) => {
    setChk(e.target.value);
}} />

</ul>

<ul>
<label>Image:</label>

<input id='imginp' type="file" name='image'  onChange={(e)=>{
    setImg(e.target.value);
}} />

</ul>

<ul>
<button id='ctbl' onClick={submittbl} >Create Table</button>

<button id='cancel'  >Cancel</button>
</ul>

        </div>


      </form>

      <table class="table">
              <thead>
                <tr>
                  <th scope="col">Layout</th>
                  <th scope="col">Name</th>
                  <th scope="col">Capacity</th>
                  <th scope="col">Status</th>
                  <th scope="col">Image</th>
                </tr>
              </thead>
              <tbody>
               {
                   userlist.map((val) =>{
                       return  <tr>
                       <td>{val.layout}</td>
                       <td>{val.name}</td>
                       <td>{val.capacity}</td>
                       <td>-</td>
                       <td><img src='{val.img}' alt='image'/></td>

                       <td><button onClick={() => {deleteinfo(val.name)}} className='btn btn-danger'>Delete</button></td>

                       <td><button className='btn btn-secondary' >Update</button></td>
                     </tr>
                   })
               }
                
              </tbody>
            </table>

    





    </div>
  )
}

export default Formfields