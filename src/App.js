import React from 'react';
import './App.css';
import Formfields from './components/Form/Formfields';

function App() {
  return (
    <div >
     
     <Formfields />
     
    </div>
  );
}

export default App;
